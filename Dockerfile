FROM mbbteam/mbb_workflows_base:latest as alltools

RUN apt -y update && apt install -y fastqc=0.11.5+dfsg-6

RUN cd /opt/biotools \
 && git clone https://github.com/voutcn/megahit.git \
 && cd megahit \
 && git submodule update --init \
 && mkdir build && cd build \
 && cmake .. -DCMAKE_BUILD_TYPE=Release \
 && make -j8 \
 && make simple_test \
 && make install \
 && cd /opt/biotools \
 && rm -rf megahit

RUN pip3 install biopython==1.69

RUN pip3 install ete3==3.1.2

RUN pip3 install scipy

RUN apt-get install -y bioperl

ENV PATH /opt/biotools/circos_current/bin:$PATH 
RUN apt-get install -y circos \
 && cd /opt/biotools && wget http://circos.ca/distribution/circos-current.tgz \
 && tar -xzf circos-current.tgz \
 && rm -rf circos-current.tgz \
 && mv circos* circos_current \
 && sed -i 's/max_points_per_track.*/max_points_per_track = 40000/' /opt/biotools/circos_current/etc/housekeeping.conf

RUN cd /opt/biotools \
 && wget https://github.com/lh3/bwa/releases/download/v0.7.17/bwa-0.7.17.tar.bz2 \
 && tar -xvjf bwa-0.7.17.tar.bz2 \
 && cd bwa-0.7.17 \
 && make -j 10 \
 && mv bwa ../bin/ \
 && cd .. \
 && rm -r bwa-0.7.17 bwa-0.7.17.tar.bz2

RUN cd /opt/biotools \
 && wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 \
 && tar -xvjf samtools-1.9.tar.bz2 \
 && cd samtools-1.9 \
 && ./configure && make \
 && cd .. \
 && mv samtools-1.9/samtools bin/samtools \
 && rm -r samtools-1.9 samtools-1.9.tar.bz2

ENV PATH /opt/biotools/release_MitoZ_v2.4-alpha:$PATH 
RUN cd /opt/biotools \
 && wget -c https://github.com/linzhi2013/MitoZ/raw/master/version_2.4-alpha/release_MitoZ_v2.4-alpha.tar.bz2 \
 && tar -jxvf release_MitoZ_v2.4-alpha.tar.bz2 \
 && rm release_MitoZ_v2.4-alpha.tar.bz2 \
 && sed -i 's/choices=\["Chordata", "Arthropoda"\]/choices=\["Chordata", "Arthropoda", "Echinodermata", "Annelida-segmented-worms", "Bryozoa", "Mollusca", "Nematoda", "Nemertea-ribbon-worms", "Porifera-sponges"\]/' /opt/biotools/release_MitoZ_v2.4-alpha/MitoZ.py \
 && python3 -c "from ete3 import NCBITaxa;ncbi = NCBITaxa();ncbi.update_taxonomy_database()"

RUN apt-get install -y ncbi-tools-bin

RUN apt-get install -y openjdk-8-jre-headless

RUN apt-get install -y xvfb

ENV PATH /opt/biotools/IGV_Linux_2.6.2:$PATH 
RUN cd /opt/biotools \
 && wget -c https://data.broadinstitute.org/igv/projects/downloads/2.6/IGV_Linux_2.6.2.zip \
 && unzip IGV_Linux_2.6.2.zip \
 && rm IGV_Linux_2.6.2.zip \
 && sed -i 's/-Xmx4g/-Xmx16g/' /opt/biotools/IGV_Linux_2.6.2/igv.sh

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

