#!/bin/bash
#This script will help a deployment of a docker image on your local machine

if [ $# -lt 2 ]
then
  echo usage : $0 dataDir resultsDir '[dockerHub|local]'
  exit
fi

# Exit on error with message
exit_on_error() {
    exit_code=$1
    last_command=${@:2}
    if [ $exit_code -ne 0 ]; then
        >&2 echo "\"${last_command}\" command failed with exit code ${exit_code}."
        exit $exit_code
    fi
}

# enable !! command completion
set -o history -o histexpand

#essayer une plage de ports entre 8787 et 8800
#APP_PORT=$2 
APP_PORT=8787
while [[ $(ss -tulw | grep $APP_PORT) != "" && $APP_PORT < 8800 ]]
do
  APP_PORT=$(( $APP_PORT + 1))
done

if [[ $(ss -tulw | grep $APP_PORT) != "" ]]
then
  echo "No tcp port available !!"
  exit -1
fi

# Docker volumes
# MBB Workflows reads data from /Data and write results to /Results

if [ $SUDO_USER ]; then realUSER=$SUDO_USER; else realUSER=`whoami`; fi

Data=$1
Results=$2

mkdir -p $Data
mkdir -p $Results

DOCK_VOL+=" --mount type=bind,src=$Data,dst=/Data"
DOCK_VOL+=" --mount type=bind,src=$Results,dst=/Results"

if [ $# -lt 3 ]
then
    APP_IMG="mbbteam/mitoassemblermegahit:latest"  
else 
    IMG_SRC=$3
    case $IMG_SRC in
        dockerHub )
            APP_IMG="mbbteam/mitoassemblermegahit:latest"  ;;
        local)
            docker build . -t mitoassemblermegahit:latest
            APP_IMG="mitoassemblermegahit:latest" ;;
        mbb)    
            #APP_IMG="X.X.X.X:5000/mitoassemblermegahit:latest" ;;
    esac
fi

IMG_NAME=$(echo $APP_IMG"-"$APP_PORT | sed s/:/-/ )
CONTAINER_ID=$( docker run --rm -d --name $IMG_NAME -p $APP_PORT:3838 $DOCK_VOL $APP_IMG )
if [ $CONTAINER_ID ]
then
    echo " "
    echo You have to put your Data on  : $1
    echo " "
    echo Results will be written to    : $2
    echo " "
    echo localhost | awk -v port=$APP_PORT '{print "You can access the shiny workflow interface at :  http://"$1":"port}'
    echo " "
    echo To start a Bash session inside the container : docker exec -it $IMG_NAME /bin/bash
else
    echo Failed to run the docker container
fi
