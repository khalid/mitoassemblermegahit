save_params <- function(path_param){
	res = ""	# 	Page : global_params
		res = paste0(res , paste("global_params:", paste0('"', input$selectglobal_params, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$results_dir))) {
			res = paste0(res, paste("results_dir:", format(input$results_dir, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("results_dir:", paste0('"', input$results_dir, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$sample_dir))) {
			res = paste0(res, paste("sample_dir:", format(input$sample_dir, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("sample_dir:", paste0('"', input$sample_dir, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$SeOrPe))) {
			res = paste0(res, paste("SeOrPe:", input$SeOrPe, "\n", sep = " "))
		} else {
			res = paste0(res, paste("SeOrPe:", paste0('"', input$SeOrPe, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$memo))) {
			res = paste0(res, paste("memo:", format(input$memo, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("memo:", paste0('"', input$memo, '"'), "\n", sep = " "))
		}	

	# 	Page : quality_check
		res = paste0(res , paste("quality_check:", paste0('"', input$selectquality_check, '"'), "\n", sep = " "))
	# 	Page : assembling
		res = paste0(res , paste("assembling:", paste0('"', input$selectassembling, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$assembling__megahit_threads))) {
			res = paste0(res, paste("assembling__megahit_threads:", format(input$assembling__megahit_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("assembling__megahit_threads:", paste0('"', input$assembling__megahit_threads, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$assembling__megahit_min_contig_len))) {
			res = paste0(res, paste("assembling__megahit_min_contig_len:", format(input$assembling__megahit_min_contig_len, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("assembling__megahit_min_contig_len:", paste0('"', input$assembling__megahit_min_contig_len, '"'), "\n", sep = " "))
		}	

	# 	Page : find_mitoscaf
		res = paste0(res , paste("find_mitoscaf:", paste0('"', input$selectfind_mitoscaf, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$find_mitoscaf__mitoz_findmitoscaf_threads))) {
			res = paste0(res, paste("find_mitoscaf__mitoz_findmitoscaf_threads:", format(input$find_mitoscaf__mitoz_findmitoscaf_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("find_mitoscaf__mitoz_findmitoscaf_threads:", paste0('"', input$find_mitoscaf__mitoz_findmitoscaf_threads, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$find_mitoscaf__mitoz_findmitoscaf_clade))) {
			res = paste0(res, paste("find_mitoscaf__mitoz_findmitoscaf_clade:", input$find_mitoscaf__mitoz_findmitoscaf_clade, "\n", sep = " "))
		} else {
			res = paste0(res, paste("find_mitoscaf__mitoz_findmitoscaf_clade:", paste0('"', input$find_mitoscaf__mitoz_findmitoscaf_clade, '"'), "\n", sep = " "))
		}	

	# 	Page : annotate
		res = paste0(res , paste("annotate:", paste0('"', input$selectannotate, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$annotate__mitoz_annotate_threads))) {
			res = paste0(res, paste("annotate__mitoz_annotate_threads:", format(input$annotate__mitoz_annotate_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("annotate__mitoz_annotate_threads:", paste0('"', input$annotate__mitoz_annotate_threads, '"'), "\n", sep = " "))
		}	

	# 	Page : visualize
		res = paste0(res , paste("visualize:", paste0('"', input$selectvisualize, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$visualize__igv_visualize_threads))) {
			res = paste0(res, paste("visualize__igv_visualize_threads:", format(input$visualize__igv_visualize_threads, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("visualize__igv_visualize_threads:", paste0('"', input$visualize__igv_visualize_threads, '"'), "\n", sep = " "))
		}	

	a = yaml.load_file("/workflow/params.total.yml", handlers=list("float#fix"=function(x){ format(x,scientific=F)}))
	p = a[["params"]]
	a["params"] = NULL
	b = yaml.load(res, handlers=list("float#fix"=function(x){ format(x,scientific=F)}))
	pnotb = subset(names(p), !(names(p)%in%names(b)))
	d = list()
	d$params = c(p[pnotb],b)
	logical = function(x) {
		result <- ifelse(x, "True", "False")
		class(result) <- "verbatim"
		return(result)
	}
	d = c(d,a)
	write_yaml(d,path_param,handlers=list(logical = logical,"float#fix"=function(x){ format(x,scientific=F) }))
	}

force_rule <- function(force_from){
	if (input$force_from=="none"){
		return("")
	}
	else if (input$force_from=="all"){ return("--forcerun all") }
	else {
		params = yaml.load_file(paste0(input$results_dir,"/params.yml"), handlers=list("float#fix"=function(x){ format(x,scientific=F)}))
		outputs = params[["outputs"]]
		tool = params[["params"]][[force_from]]
		steptool = paste0(force_from,"__",tool)
		if (length(outputs[[steptool]])==1)
			rule = names(outputs[[steptool]])[[1]]
		else{
			rule = names(outputs[[steptool]])[[grep(input$SeOrPe,names(outputs[[steptool]]))]]
		}
		return(paste0("--forcerun ",rule))
	}
}
#' Event when use RULEGRAPH button
observeEvent({c(input$sidebarmenu,input$refresh_rg)}, {

	if (!dir.exists(paste0(input$results_dir,"/logs"))){
		dir.create(paste0(input$results_dir,"/logs"))
	}
	if(input$sidebarmenu=="RULEGRAPH"){
		input_list <- reactiveValuesToList(input)
		toggle_inputs(input_list,F,F)
		path_param <- paste0(input$results_dir,"/params.yml")

		save_params(path_param)
		i = sample.int(1000,size = 1)

		system(paste0("rm ",input$results_dir,"/rulegraph*"))

		outUI = tryCatch({
			system(paste0("snakemake -s /workflow/Snakefile --configfile ",input$results_dir,"/params.yml -d ",input$results_dir," all --rulegraph 1> ",input$results_dir,"/rulegraph",i,".dot 2> ",input$results_dir,"/logs/rulegraph.txt"),intern=T)
			system(paste0("cat ",input$results_dir,"/rulegraph",i,".dot | dot -Tsvg -Gratio=0.75 > ",input$results_dir,"/rulegraph",i,".svg"),intern=T)
			tagList(img(src = paste0("results/rulegraph",i,".svg") ,alt = "Rulegraph of Snakemake jobs",style="max-width: 100%;height: auto;display: block;margin: auto"))},
		error = function(e){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(HTML(paste(readLines(paste0(input$results_dir,"/logs/rulegraph.txt"),warn=F), collapse = "<br/>"))))},
		warning = function(w){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(HTML(paste(readLines(paste0(input$results_dir,"/logs/rulegraph.txt"),warn=F), collapse = "<br/>"))))})
		addResourcePath("results", input$results_dir)
		output$RULEGRAPH_svg = renderUI(outUI)
		toggle_inputs(input_list,T,F)
}})
observeEvent({c(input$sidebarmenu,input$dry_run_button)}, {

	if (!dir.exists(paste0(input$results_dir,"/logs"))){
		dir.create(paste0(input$results_dir,"/logs"))
	}
	if(input$sidebarmenu=="RULEGRAPH"){
		input_list <- reactiveValuesToList(input)
		toggle_inputs(input_list,F,F)
		path_param <- paste0(input$results_dir,"/params.yml")

		save_params(path_param)

		force = force_rule(input$force_from)
		dry_out = system(paste0("snakemake -s /workflow/Snakefile --configfile ", paste0(input$results_dir,"/params.yml") ,	" -d ", input$results_dir ,	" --cores ", input$cores, " all ", force, " -n -p"),wait = T, intern=T)
		dryUI = tags$p(HTML(paste(dry_out, collapse = "<br/>")))

		output$dry_run_div = renderUI(dryUI)
		toggle_inputs(input_list,T,F)
	}})
#' Event when use RunPipeline button
observeEvent(input$RunPipeline, {

	rv$running = T
	input_list <- reactiveValuesToList(input)
	toggle_inputs(input_list,F,F)
	updateTabsetPanel(session, "sidebarmenu", selected = "run_out")
	path_param <- paste0(input$results_dir,"/params.yml")

		save_params(path_param)


	outUI = tryCatch({
		if (!dir.exists(paste0(input$results_dir,"/logs"))){
			dir.create(paste0(input$results_dir,"/logs"))
		}
		if (!file.exists(paste0(input$results_dir,"/logs/runlog.txt"))){
			file.create(paste0(input$results_dir,"/logs/runlog.txt"))
		}
		if (file.exists(paste0(input$results_dir,"/logs/workflow_end.ok"))){
			file.remove(paste0(input$results_dir,"/logs/workflow_end.ok"))
		}
		if (file.exists(paste0(input$results_dir,"/logs/workflow_end.error"))){
			file.remove(paste0(input$results_dir,"/logs/workflow_end.error"))
		}
		system(paste0("touch ",input$results_dir,"/logs/workflow.running"),wait = T)
			system(paste0("snakemake -s /workflow/Snakefile --configfile ",input$results_dir,"/params.yml -d ",input$results_dir," all --rulegraph | dot -Tpng -Gratio=0.75 > ",input$results_dir,"/Rule_graph_mqc.png"),wait = T)
		force = force_rule(input$force_from)
		rerun = if (input$rerun_incomplete) "--rerun-incomplete" else ""
			system(paste0("snakemake -s /workflow/Snakefile --configfile ", paste0(input$results_dir,"/params.yml") ," -d ", input$results_dir ," --cores ", input$cores, " all ", force, " ", rerun, " -n -p > ", input$results_dir, "/Run_Summary_mqc.html"),wait = T)
			system(paste0("sed -i '1 i\\<pre> ' ",input$results_dir,"/Run_Summary_mqc.html"))
		system2("python3",paste0("-u -m snakemake -s /workflow/Snakefile --configfile ", paste0(input$results_dir,"/params.yml") ,	" -d ", input$results_dir ,	" --cores ", input$cores, " all ", force, " ",rerun),wait = FALSE, stdout = paste0(input$results_dir,"/logs/runlog.txt"), stderr = paste0(input$results_dir,"/logs/runlog.txt"))
		tags$iframe(src="results/multiqc_report.html",width="100%", height="900px")},
		error = function(e){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(paste0("error : ",e$message)))},
		warning = function(w){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(paste0("error : ",w$message)))})
		})

		shinyDirChoose(input, "shinydir_results_dir", root=c(Results="/Results"),session = session)
		observeEvent({parseDirPath(c(Results="/Results"),input$shinydir_results_dir)},{
			updateTextInput(session,"results_dir",value = parseDirPath(c(Results="/Results"),input$shinydir_results_dir))
		})

		shinyDirChoose(input, "shinydir_sample_dir", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseDirPath(c(Data="/Data",Results="/Results"),input$shinydir_sample_dir)},{
			updateTextInput(session,"sample_dir",value = parseDirPath(c(Data="/Data",Results="/Results"),input$shinydir_sample_dir))
		})


